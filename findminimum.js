const data = [-8000, -9000, -2000];

let min = Number.MAX_SAFE_INTEGER;

for (const item of data) {
    if (item < min) {
        min = item;
    }
}

console.log(data);