function isPalindrome(string) {
    for(i = 0; i<string.length/2; i++) {
    if(string.charAt(i) != string.charAt(string.length-1-i)) {
        return false;
    }
}

    return true;
}

console.log(isPalindrome ("KATAK"));
console.log(isPalindrome("MATARAMMARATAM"));
console.log(isPalindrome("palindrome"));
console.log(isPalindrome("KODOK"));
console.log(isPalindrome("MALAM"));
console.log(isPalindrome("tenet"));
console.log(isPalindrome("mama"));