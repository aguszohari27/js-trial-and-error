const test = [1, 2, 3, 4, 5];

function sum(arr) {
  // base case
  console.log(arr);
  console.log(arr[0]);
  console.log();
  if (arr.length === 0) {
    return;
  }
  // work toward base case
  // recursive call
  return arr[0] + sum(arr.slice(1)); // array.shift() akan menghilangkan index 0 dari suatu array
}

console.log(sum(test));

console.log(arr[0]);
console.log();